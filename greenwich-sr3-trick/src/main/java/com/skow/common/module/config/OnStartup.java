package com.skow.common.module.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.integration.context.IntegrationContextUtils;
import org.springframework.stereotype.Component;

@Component
public class OnStartup implements ApplicationListener<ContextRefreshedEvent> {

    private final DefaultListableBeanFactory context;

    @Autowired
    public OnStartup(DefaultListableBeanFactory context) {
        this.context = context;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (context != null && context
            .containsBean(IntegrationContextUtils.MESSAGE_HANDLER_FACTORY_BEAN_NAME)) {
            context.removeBeanDefinition(IntegrationContextUtils.MESSAGE_HANDLER_FACTORY_BEAN_NAME);
        }
    }
}

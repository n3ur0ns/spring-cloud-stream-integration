package com.skow.common.instance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.SendTo;

@Configuration
@EnableAutoConfiguration
@ComponentScan(
    basePackages = {
        "com.skow.common.module",
    },
    excludeFilters = {
    },
    basePackageClasses = {
    }
)
@EnableBinding(Processor.class)
public class Consumer {

    public static void main(String[] args) {
        SpringApplication.run(Consumer.class, args);
    }

    @StreamListener(Processor.INPUT)
    @SendTo("from.producer")
    public Message<?> inputBridge(Message<?> request) {
        return MessageBuilder.withPayload(request.getPayload())
            .copyHeaders(request.getHeaders())
            .build();
    }

}

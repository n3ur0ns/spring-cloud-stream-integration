package com.skow.common.instance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.handler.BridgeHandler;

@Configuration
@EnableAutoConfiguration
@ComponentScan(
    basePackages = {
        "com.skow.common.module",
    },
    excludeFilters = {
    },
    basePackageClasses = {
    }
)
@EnableBinding(Processor.class)
public class Producer {

    public static void main(String[] args) {
        SpringApplication.run(Producer.class, args);
    }

    @Bean
    @ServiceActivator(inputChannel = "to.consumer")
    public BridgeHandler outputBridge() {
        BridgeHandler bridge = new BridgeHandler();
        bridge.setOutputChannelName(Processor.OUTPUT);
        return bridge;
    }

}

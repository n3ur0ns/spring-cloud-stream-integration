package com.skow.common.test.integration;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.skow.common.instance.Consumer;
import com.skow.common.instance.Producer;
import com.skow.common.module.model.DTO;
import com.skow.common.module.model.Property;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = {
        Producer.class,
        Consumer.class,
    }
)
@ActiveProfiles(
    profiles = {
        "producer",
        "consumer",
    }
)
@TestPropertySource(
    properties = {
        "spring.main.allow-bean-definition-overriding = true",
    }
)
@Ignore
public class AbstractProducerToConsumerTest {

    @Autowired
    @Qualifier("input.channel")
    MessageChannel inputChannel;
    @Autowired
    Source exchangeTopicOutput;
    @Autowired
    Sink exchangeTopicInput;
    @Autowired
    @Qualifier("output.channel")
    QueueChannel queueChannel;
    @Autowired
    MessageCollector messageCollector;

    public void run() {
        Property property = new Property();
        property.setProperty("property");
        DTO dto = new DTO();
        dto.setName("name");
        dto.setProperty(property);

        Message messageToConsumer = MessageBuilder.withPayload(dto)
            .build();
        inputChannel.send(messageToConsumer);

        Message<String> messageCollected = (Message<String>) messageCollector
            .forChannel(exchangeTopicOutput.output()).poll();

        exchangeTopicInput.input().send(
            MessageBuilder.withPayload(messageCollected.getPayload().getBytes())
                .copyHeaders(messageCollected.getHeaders())
                .build()
        );
        Message messageFromProducer = queueChannel.receive(0);

        assertThat(messageFromProducer.getHeaders().get("PAYLOAD"))
            .isExactlyInstanceOf(DTO.class);
    }
}

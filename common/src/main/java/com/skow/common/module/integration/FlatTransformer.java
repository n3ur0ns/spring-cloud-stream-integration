package com.skow.common.module.integration;

import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.skow.common.module.model.DTO;

@Component
public class FlatTransformer {

    @Transformer
    public Message<DTO> transform(Message<DTO> message) {
        return message;
    }
}

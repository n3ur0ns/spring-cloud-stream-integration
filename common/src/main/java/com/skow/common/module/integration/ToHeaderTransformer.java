package com.skow.common.module.integration;

import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.skow.common.module.model.DTO;

@Component
public class ToHeaderTransformer {

    @Transformer
    public Message transform(Message<DTO> message) {
        DTO dto = message.getPayload();

        return MessageBuilder.withPayload("")
            .copyHeaders(message.getHeaders())
            .setHeader("PAYLOAD", dto)
            .build();
    }
}

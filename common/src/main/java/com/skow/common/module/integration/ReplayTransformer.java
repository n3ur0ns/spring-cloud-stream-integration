package com.skow.common.module.integration;

import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
@ManagedResource
public class ReplayTransformer {

    @Transformer
    public Message<?> transform(Message<?> message) {
        return MessageBuilder.withPayload(message.getPayload())
            .copyHeaders(message.getHeaders())
            .setHeader("REDO_BEAN", "replayTransformer")
            .setHeader("REDO_PAYLOAD", message.getPayload())
            .build();
    }

}

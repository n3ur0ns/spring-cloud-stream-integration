### Context

We are using spring-integration for 2 different instances, a producer and a consumer, and spring-cloud-stream is between them.
We use a single exchange topic (in the broker) to send different payload object types between the two instances.

### We experienced the issue when we tried to upgrade from Greenwich.SR1 to SR2/SR3.

```shell
2019-10-25 16:15:01.198  INFO 88619 --- [           main] o.s.i.h.s.MessagingMethodInvokerHelper   : Overriding default instance of MessageHandlerMethodFactory with provided one.
[ERROR] Tests run: 1, Failures: 0, Errors: 1, Skipped: 0, Time elapsed: 3.532 s <<< FAILURE! - in com.skow.common.test.integration.ProducerToConsumerTest
[ERROR] test(com.skow.common.test.integration.ProducerToConsumerTest)  Time elapsed: 0.313 s  <<< ERROR!
org.springframework.integration.transformer.MessageTransformationException: Failed to transform Message; nested exception is org.springframework.messaging.MessageHandlingException: nested exception is java.lang.ClassCastException: java.util.LinkedHashMap cannot be cast to com.skow.common.module.model.DTO, failedMessage=GenericMessage [payload={name=name, property={property=property}}, headers={replyChannel=transform.channel, id=346999f9-ee12-4185-211d-a3a74178d77e, REDO_BEAN=replayTransformer, contentType=application/json, REDO_PAYLOAD={name=name, property={property=property}}, timestamp=1572012901198}]
        at com.skow.common.test.integration.ProducerToConsumerTest.test(ProducerToConsumerTest.java:9)
Caused by: org.springframework.messaging.MessageHandlingException: nested exception is java.lang.ClassCastException: java.util.LinkedHashMap cannot be cast to com.skow.common.module.model.DTO
        at com.skow.common.test.integration.ProducerToConsumerTest.test(ProducerToConsumerTest.java:9)
Caused by: java.lang.ClassCastException: java.util.LinkedHashMap cannot be cast to com.skow.common.module.model.DTO
        at com.skow.common.test.integration.ProducerToConsumerTest.test(ProducerToConsumerTest.java:9)
```

### After analyzing code history, we found the below commit is responsible 

```shell
diff --git a/spring-cloud-stream/src/main/java/org/springframework/cloud/stream/config/BinderFactoryConfiguration.java b/spring-cloud-stream/src/main/java/org/springframework/cloud/stream/config/BinderFactoryConfiguration.java
index f76535aa..47acf7c4 100644
 
-       @Bean
+       @Bean(IntegrationContextUtils.MESSAGE_HANDLER_FACTORY_BEAN_NAME)
        public static MessageHandlerMethodFactory messageHandlerMethodFactory(
                        CompositeMessageConverterFactory compositeMessageConverterFactory,
                        @Qualifier(IntegrationContextUtils.ARGUMENT_RESOLVERS_BEAN_NAME) HandlerMethodArgumentResolversHolder ahmar,
```

### Sample project demoing the issue

```shell
git clone https://gitlab.com/n3ur0ns/spring-cloud-stream-integration.git
cd spring-cloud-stream-integration
./mvnw clean verify -P IT -fn 
```

### We found a trick as shown in submodule greenwich-sr3-trick

```java
@Component
public class OnStartup implements ApplicationListener<ContextRefreshedEvent> {

    private final DefaultListableBeanFactory context;

    @Autowired
    public OnStartup(DefaultListableBeanFactory context) {
        this.context = context;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (context != null && context
            .containsBean(IntegrationContextUtils.MESSAGE_HANDLER_FACTORY_BEAN_NAME)) {
            context.removeBeanDefinition(IntegrationContextUtils.MESSAGE_HANDLER_FACTORY_BEAN_NAME);
        }
    }
}
```
